################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Dictionary.c \
../src/Trim.c \
../src/UnsortedDictionary.c \
../src/WordCounterApp.c 

OBJS += \
./src/Dictionary.o \
./src/Trim.o \
./src/UnsortedDictionary.o \
./src/WordCounterApp.o 

C_DEPS += \
./src/Dictionary.d \
./src/Trim.d \
./src/UnsortedDictionary.d \
./src/WordCounterApp.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



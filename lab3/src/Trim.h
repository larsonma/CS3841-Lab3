/**
 * This file contains the interface for the Trim utility. This file provides easy functions for
 * cleaning up word before adding them to a dictionary
 * @file Trim
 * @author Mitchell Larson
 * @date 9/26/2017
 */

#ifndef SRC_TRIM_H_
#define SRC_TRIM_H_

/**
 * This function accepts a string and removes any non-alphabetic characters. This function will not change the
 * contents of the incoming string.
 * @param text	A non-null pointer to a string
 * @return		A dynamically allocated pointer to a word. NULL if input string is NULL.
 */
char* stripNonText(const char* text);

/**
 * This function takes all lower case letters in a string and changes them to upper case. Non alphabetic characters
 * are ignored and left unchanged.
 * @param text	A non-null pointer to a string.
 * @return		A dynamically allocated pointer to string. NULL if input string is NULL.
 */
char* toCaps(const char* text);

#endif /* SRC_TRIM_H_ */

/**
 * This file contains the interface for the linked list file.
 * Complete the comments to be compliant with Doxygen practices.
 * @file linkedlist.h
 * @author Mitchell Larson
 * @date 9/17/2017
 */
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <stdbool.h>
#include <stdint.h>


struct  listNode {
  void* data;
  uint32_t dataSize;
  struct listNode* nextNode;
  struct listNode* prevNode;
};

struct linkedList
{
  struct listNode* head;
  struct listNode* tail;
  uint32_t size;

};

struct tuple {
	void* data;
	void* nextData;
};

struct linkedListIterator
{
  struct listNode* current;
};

/**
 * This function accepts a linkedList pointer and initializes the state of the linkedList. This should not
 * be called without a prior call to ll_clear() if the list has already been initialized.
 * @param list	A non-null pointer to a linkedList structure.
 */
void ll_init(struct linkedList* list);

/**
 * This function adds a new node to the linked list. The size of the list is updates, and references between nodes are
 * updated if necessary. This function will handle all memory checks as well to ensure safe data allocation.
 * @param list		pointer to a LinkedList structure.
 * @param object	pointer to and object of any type.
 * @param size		size of the object that the object pointer points to.
 * @return			true if the object is successfully added to the list, false otherwise.
 */
bool ll_add(struct linkedList* list, const void* object, uint32_t size);

/**
 * This function will add a new node to the given linkedList containing the given data to the given index.
 * @param list		A non-null pointer to the linkedList that will hold the new node.
 * @param object	A non-null pointer to the data the new node will contain.
 * @param size		The size of the data object.
 * @param index		The index to insert the new data at within the linkedList.
 * @return			true is returned if the insertion was successful, false otherwise.
 */
bool ll_addIndex(struct linkedList* list, const void* object, uint32_t size, uint32_t index);

/**
 * This function will remove the node at a given index from a given linkedList. This function frees all memory
 * associated with the node to be removed, adjusts all node references for the linkedList, and decrements the
 * size of the linkedList.
 * @param list		A non-null pointer to the list that the node will be removed from.
 * @param index		The index of the linkedList that the node will be removed from.
 * @return			true is returned if the node is successfully returned, false otherwise.
 */
bool ll_remove(struct linkedList* list, uint32_t index);

/**
 * This function will get the pointer to data for the node of a given linkedList at a given index.
 * @param list		A non-null pointer to a linkedList.
 * @param index		The index of the node that data will be retrieved from.
 * @return			A pointer to the node's data will be returned if successful, NULL is returned otherwise.
 */
void* ll_get(struct linkedList* list, uint32_t index);

struct tuple* ll_getTwo(struct linkedList* list, uint32_t index);

/**
 * This function will clear all the data from a given linkedList and reset the list to initialized values.
 * @param list	A non-null pointer to a linkedList.
 */
void ll_clear(struct linkedList* list);

/**
 * This function retrieves the size of a given linked list.
 * @param list	A non-null pointer to a linkedList structure.
 * @return		The size of the list. If a list pointer is null, 0 is returned.
 */
uint32_t ll_size(struct linkedList* list);

/**
 * This function will create an iterator for a provided linkedList. Upon creation of an iterator, the head will
 * point to the head of the list. To obtain data from the head of the list, ll_next() should be called.
 * @param list	A non-null pointer to a list structure.
 * @return		A pointer to a linkedList iterator will be returned if successful. If unsuccessful, null is returned.
 */
struct linkedListIterator* ll_getIterator(struct linkedList* list);

/**
 * This function will indicate if ll_next() can be called on the given iterator.
 * @param iter	A non-null pointer to a iterator structure.
 * @return		true if ll_next() can be called on the iterator, false otherwise.
 */
bool ll_hasNext(struct linkedListIterator* iter);

/**
 * This function will return the data stored in the next node of the linkedList if it exists.
 * @param iter	A non-null pointer to an iterator for a linkedList.
 * @return		A pointer to the object data of the next node if it exists; false otherwise.
 */
void* ll_next(struct linkedListIterator* iter);










#endif

/*
 * Dictionary.c
 * This is a wrapper around the linkedlist struct to provide an efficient and fast method of adding and retrieving text. This file allows the linked list to be sorted in
 * alphabetical order. Alphabetical sorting enables binary adds and gets, greatly increasing the performance over an ordinary linkedlist.
 *  Created on: Sep 21, 2017
 *      Author: cs3841
 */

#include "Dictionary.h"
#include "linkedlist.h"
#include <stdbool.h>
#include <math.h>

static struct worddata* initWordData(char* word);
static int dict_findIndex(struct dictionary* dict, char* word);

/**
 * This function adds a word to the dictionary that is passed in. If the word exists, then the count of the word is incremented by 1. If a
 * does not exist, then the word is added to a new worddata structure that is added alphabetically to the dictionary.
 * @param dict		a non-null pointer to a dictionary.
 * @param word		a non-null pointer to a word.
 */
void addWord(struct dictionary* dict, char* word) {
	//make sure the dictionary and word are not null
	if (word != NULL && dict != NULL) {

		//if the dictionary is empty, add the wordData and initialize the wordData count to 1.
		if (ll_size(dict->dictionary) == 0) {
			struct worddata* wordData = initWordData(word);

			if (wordData != NULL)
				ll_add(dict->dictionary, wordData, sizeof(struct worddata*));
		} else {
			struct worddata* wordData = findWord(dict, word);

			//if the word exists, increment it's count.
			if(wordData != NULL){
				incrementCount(wordData);
			}else{
				//if the word does not exist, create a new word and add it alphabetically to the dictionary.
				struct worddata* wordData = initWordData(word);

				if(wordData != NULL){
					int index = -1 * dict_findIndex(dict, word);
					ll_addIndex(dict->dictionary, wordData, sizeof(wordData), index);
				}
			}
		}
	}
}

/**
 * This function destructs and frees all allocated memory associated with a dictionary structure.
 * @param dict		a non-null pointer to a dictionary.
 */
void cleanUp(struct dictionary* dict) {
	if(dict != NULL){
		ll_clear(dict->dictionary);
		free(dict);
	}
}

void combineCounts(struct worddata* word){

}

/**
 * This function will search a dictionary structure for a specified word. The search is implemented using a
 * binary search implementation. If the word is not found, then NULL is returned to the caller.
 * @param dict		a non-null pointer to a dictionary.
 * @param word		a non-null pointer to a word.
 * @return			a pointer to the found worddata, or NULL if not found.
 */
struct worddata* findWord(struct dictionary* dict, char* word){
	if(dict != NULL && word != NULL){
		int index = dict_findIndex(dict,word);

		if(index >= 0)
			return ll_get(dict->dictionary, index);
	}
	return NULL;
}

/**
 * This function returns the number of unique words in the dictionary
 * @param dict		a non-null pointer to a dictionary structure.
 * @return			The total number of words added to the dictionary. -1 if an error occurs.
 */
int getTotalWordCount(struct dictionary* dict){
	if(dict != NULL){
		/*int count = 0;

		struct linkedListIterator* iterator = ll_getIterator(dict->dictionary);
		if(iterator != NULL){
			while(ll_hasNext(iterator)){
				count += ((struct worddata*)ll_next(iterator))->count;
			}
			return count;
		}
		free(iterator);*/
		return ll_size(dict->dictionary);
	}
	return -1;
}

/**
 * This function will return a worddata structure at a given index of a dictionary.
 * @param dict		a non-null pointer to a dictionary.
 * @param index		a valid integer index to retrieve.
 * @return			a pointer to the worddata found or NULL if not found.
 */
struct worddata* getWord(struct dictionary* dict, uint32_t index){
	if(dict != NULL && index >= 0 && index < ll_size(dict->dictionary)){
		return ll_get(dict->dictionary, index);
	}
	return NULL;
}

/**
 * This function increments the count of a word by 1.
 * @param word	a non-null pointer to a worddata structure.
 */
void incrementCount(struct worddata* word){
	if(word != NULL){
		word->count++;
	}
}

/**
 * This function initializes a dictionary structure which is just a wrapper around a ordinary
 * linked list. This is done to make the structure type clear, and to provide protection to
 * the functions in the Dictionary.c file. The dictionary struct is returned to the caller.
 * @param dict	a non-null pointer to a dictionary structure.
 */
void initialize(struct dictionary* dict){
	if(dict != NULL){
		struct linkedList* myList = malloc(sizeof(struct linkedList*));
		ll_init(myList);
		dict->dictionary = myList;
	}
}

static struct worddata* initWordData(char* word){
	if(word != NULL){
		struct worddata* wordData = malloc(sizeof(struct worddata));

		if(wordData != NULL){
			strcpy(wordData->word, word);
			wordData->count = 1;
			return wordData;
		}
	}
	return NULL;
}

/**
 * This function will return a positive integer of the index that a word can be found at. If the word cannot be
 * found, the negative index of where the word should be inserted into the dictionary is returned.
 * @param dict	a non-null pointer to a dictionary.
 * @param word	a non-null pointer to a word.
 * @return
 */
static int dict_findIndex(struct dictionary* dict, char* word){
	if(dict != NULL && word != NULL){
		bool foundIndex = false;
		int index = (int)(ll_size(dict->dictionary) / 2);

		while(!foundIndex){
			//index = (int)(index /2);
			struct tuple* wordDataTuple = ll_getTwo(dict->dictionary, index);

			int cmpWord = strcmp(word, wordDataTuple->data);

			//if the word was found, return the index it was found at.
			if (cmpWord == 0) {
				return index;
			} else if (cmpWord > 0) {//if the word being searched for comes after the word found
				int cmpNextWord = strcmp(word, wordDataTuple->nextData);

				//if the word being searched for is found at the next index, return that index.
				if (cmpNextWord == 0) {
					return index + 1;
				} else if (cmpNextWord < 0) {		//if the word being searched for comes before the next word, indicate that the new word belongs at the next index.
					return -1 * (index + 1);
				}else {								//if the word still comes after the next word, increase the index to the current index plus half of it.
					index = index + ((int)(index/2));
				}
			} else {//if the word comes before the word at the current index, search the index that is half of the current.
				index = (int)(index/2);
			}
			free(wordDataTuple);
		}
	}
	return NAN;
}


/*
 * Word.h
 *
 *  Created on: Sep 24, 2017
 *      Author: cs3841
 */

#ifndef SRC_DICTIONARY_H_
#define SRC_DICTIONARY_H_

#include "linkedlist.h"

struct worddata {
	int32_t count;
	char word[32];
};

struct dictionary {
	struct linkedList* dictionary;
};

/**
 * This function adds a word to the dictionary that is passed in. If the word exists, then the count of the word is incremented by 1. If a
 * does not exist, then the word is added to a new worddata structure that is added to the dictionary.
 * @param dict		a non-null pointer to a dictionary.
 * @param word		a non-null pointer to a word.
 */
void addWord(struct dictionary* dict, char* word);

/**
 * This function destructs and frees all allocated memory associated with a dictionary structure.
 * @param dict		a non-null pointer to a dictionary.
 */
void cleanUp(struct dictionary* dict);

/**
 * Not Implemented
 * @param word
 */
void combineCounts(struct worddata* word);

/**
 * This function will search a dictionary structure for a specified word. The search is implemented by iterating through
 * the list until the word is found. If the word is not found, then NULL is returned to the caller.
 * @param dict		a non-null pointer to a dictionary.
 * @param word		a non-null pointer to a word.
 * @return			a pointer to the found worddata, or NULL if not found.
 */
struct worddata* findWord(struct dictionary* dict, char* word);

/**
 * This function returns the number of unique words in the dictionary
 * @param dict		a non-null pointer to a dictionary structure.
 * @return			The total number of words added to the dictionary. -1 if an error occurs.
 */
int getTotalWordCount(struct dictionary* dict);

struct worddata* getWord(struct dictionary* dict, uint32_t index);

void incrementCount(struct worddata* word);

void initialize(struct dictionary* dict);

#endif /* SRC_DICTIONARY_H_ */
